package com.BookStore;

import com.BookStore.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.BookStore.service.IService;

@SpringBootApplication
public class Application implements CommandLineRunner {
	
	@Autowired
	private IService<Book> service;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

        for (int i = 0; i < 100; i++) {

            Book book = new Book();
            book.setTitle("Spring Microservices in Action, Part: " + i);
            book.setAuthor("John Carnell");
            book.setCoverPhotoURL("https://static01.helion.com.pl/global/okladki/326x466/c8c229d50062f5599b8a7f26a471b24a,s_00ae.jpg");
            book.setIsbnNumber(1617293989L);
            book.setPrice(Math.random()*100);
            book.setLanguage("English");
            service.saveOrUpdate(book);

        }
    }
}